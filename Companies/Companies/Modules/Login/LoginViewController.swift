//
//  LoginViewController.swift
//  Companies
//
//  Created by Filipe Oliveira on 18/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField! {
        willSet {
            newValue.leftView = UIImageView(image: #imageLiteral(resourceName: "icEmail"))
            newValue.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var passwordTextField: UITextField! {
        willSet {
            newValue.leftView = UIImageView(image: #imageLiteral(resourceName: "icCadeado"))
            newValue.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var loginButton: UIButton! {
        willSet {
            newValue.layer.cornerRadius = 5.0
        }
    }
    
    @IBAction func loginWasTapped() {
        
        indicator(show: true)
        
//        let email = "testeapple@ioasys.com.br"
//        let password = "12341234"
        
        let email = emailTextField.text ?? .init()
        let password = passwordTextField.text ?? .init()
        
        CompanyClient.login(email: email, password: password) { response, error, cache in

            self.indicator(show: false)
            
            guard let homeViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController(), response != nil else { return }
            
            User.shared = response

            if #available(iOS 13.0, *) {
                sceneDelegate?.switchRootViewController(viewController: homeViewController, animated: true)
            } else {
                appDelegate?.switchRootViewController(viewController: homeViewController, animated: true)
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        }
        
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        indicator(show: false)
    }
    
    private func indicator(show: Bool) {
        view.isUserInteractionEnabled = !show
        show ? indicatorView.startAnimating() : indicatorView.stopAnimating()
        loginButton.setTitle(!show ? "ENTRAR" : nil, for: .normal)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            loginWasTapped()
        }
        
        return true
    }
}
