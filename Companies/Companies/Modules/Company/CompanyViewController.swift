//
//  CompanyViewController.swift
//  Companies
//
//  Created by Filipe Oliveira on 18/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import UIKit

class CompanyViewController: UIViewController {
    
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBAction func backWasTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: false)
    }
    
    var model: Company?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = model?.name
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        initialsLabel.text = model?.initials
        descriptionLabel.text = model?.description
    }
}
