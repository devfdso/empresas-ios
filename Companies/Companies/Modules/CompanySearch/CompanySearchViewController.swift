//
//  CompanySearchViewController.swift
//  Companies
//
//  Created by Filipe Oliveira on 18/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import UIKit

let cellIdentifier = "CELL_DEFAULT"

class CompanySearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var searchRequest: CompanyClient?
    
    private var model: [Company]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var lastQuery: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    func searchCompanies(query: String) {
        
        if query != lastQuery {
            lastQuery = query
        } else {
            return
        }
        
        searchRequest?.task?.cancel()
        
        searchRequest = CompanyClient.searchCompanies(query: query) { response, error, cache in
            self.model = response
        }
    }
}

extension CompanySearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { model?.count ?? .zero }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CompanySearchTableViewCell else { return .init() }
        
        cell.model = model?[indexPath.row]
        
        return cell
    }
}

extension CompanySearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let companyNavigationController = UIStoryboard(name: "Company", bundle: nil).instantiateInitialViewController() as? UINavigationController, let companyViewController = companyNavigationController.topViewController as? CompanyViewController else { return }
        
        companyViewController.model = model?[indexPath.row]
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        present(companyNavigationController, animated: false)
    }
}
