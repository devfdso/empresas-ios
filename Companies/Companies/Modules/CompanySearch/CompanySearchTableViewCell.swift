//
//  CompanySerchTableViewCell.swift
//  Companies
//
//  Created by Filipe Oliveira on 18/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import UIKit

class CompanySearchTableViewCell: UITableViewCell {
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    var model: Company? {
        willSet {
            initialsLabel.text = newValue?.initials
            nameLabel.text = newValue?.name
            typeLabel.text = newValue?.type
            countryLabel.text = newValue?.country
        }
    }
}
