//
//  HomeViewController.swift
//  Companies
//
//  Created by Filipe Oliveira on 18/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet var titleView: UIImageView!
    @IBOutlet weak var searchLabel: UILabel!
    
    @IBOutlet weak var searchBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var logoutBarButtonItem: UIBarButtonItem!
    
    @IBAction func logoutWasTapped(_ sender: UIBarButtonItem) {
        
        guard let homeViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() else { return }
        
        let alertController = UIAlertController(title: User.shared?.name.uppercased(), message: "Deseja entrar com outro usuário?", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Sim", style: .default) { (_) in
            
            APICacheManager.shared.clearCache()
            UserDefaults.standard.set(nil, forKey: APIRequest.Constants.authenticationHeadersDefaultsKey)
            
            if #available(iOS 13.0, *) {
                sceneDelegate?.switchRootViewController(viewController: homeViewController, animated: true)
            } else {
                appDelegate?.switchRootViewController(viewController: homeViewController, animated: true)
            }
        }

        alertController.addAction(yesAction)
        alertController.addAction(UIAlertAction(title: "Não", style: .default, handler: nil))
        
        present(alertController, animated: true)
    }
    
    @IBAction func searchWasTapped(_ sender: UIBarButtonItem) {
        searchLabel.isHidden = true
        
        present(searchController, animated: true)
    }
    
    let searchController = UISearchController(searchResultsController: UIStoryboard(name: "CompanySearch", bundle: nil).instantiateInitialViewController())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleView = titleView
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        configureSearchController()
    }
    
    private func configureSearchController() {
        if #available(iOS 13.0, *) {
            searchController.automaticallyShowsSearchResultsController = true
            searchController.showsSearchResultsController = true
        }

        searchController.searchBar.setAppearance(textColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), font: UIFont(name: "Roboto", size: 14) ?? UIFont.systemFont(ofSize: 14))
        searchController.searchBar.barTintColor = #colorLiteral(red: 0.9046015143, green: 0.3775699139, blue: 0.5210194588, alpha: 1)
        searchController.searchBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchResultsUpdater = self
    }
}

extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        guard let searchResultsController = searchController.searchResultsController as? CompanySearchViewController else { return }
        
        searchResultsController.searchCompanies(query: searchController.searchBar.text ?? .init())
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchLabel.isHidden = false
    }
}
