//
//  CompanyClient.swift
//  Companies
//
//  Created by Filipe Oliveira on 20/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]
typealias JSONArray = [JSONDictionary]

final class CompanyClient: APIRequest {
    
    @discardableResult
    static func login(email: String, password: String, completion: ResponseBlock<User>?) -> CompanyClient {
        
        let request = CompanyClient(method: .post, path: "users/auth/sign_in", parameters: ["email": email, "password": password], urlParameters: nil, cacheOption: .networkOnly) { response, error, cache in
            
            if let dictionary = (response as? JSONDictionary)?["investor"] as? JSONDictionary {
                completion?(.init(dictionary: dictionary), nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func searchCompanies(query: String, completion: ResponseBlock<[Company]>?) -> CompanyClient {
        
        let request = CompanyClient(method: .get, path: "enterprises", parameters: nil, urlParameters: ["name": query], cacheOption: .networkOnly) { response, error, cache in
            
            if let dictionaries = ((response as? JSONDictionary)?["enterprises"]) as? JSONArray {
                let companies = dictionaries.map { Company(dictionary: $0) }
                completion?(companies, nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
}
