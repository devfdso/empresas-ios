//
//  Extensions.swift
//  Companies
//
//  Created by Filipe Oliveira on 20/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import UIKit

extension UIWindow {
    func switchRootViewController(viewController: UIViewController, animated: Bool) {
        self.rootViewController = viewController
        
        guard animated else {
            return
        }
        
        UIView.transition(with: self,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
}

extension UISearchBar {
    func setAppearance(textColor: UIColor,
                       font: UIFont) {
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: font]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = .init(string: "Pesquisar", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1), NSAttributedString.Key.font: font])
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Cancelar"
        
        if #available(iOS 13.0, *) {
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = #colorLiteral(red: 0.9046015143, green: 0.3775699139, blue: 0.5210194588, alpha: 1)
        } else {
            
        }
    }
}
