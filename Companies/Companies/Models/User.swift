//
//  User.swift
//  Companies
//
//  Created by Filipe Oliveira on 20/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import Foundation

final class User: Mappable {

    static var shared: User? {
        get {
            if let dictionary = UserDefaults.standard.object(forKey: "User") as? JSONDictionary {
                return .init(dictionary: dictionary)
            }
            
            return nil
        }
        set {
            if let value = newValue {
                UserDefaults.standard.set(["id": value.id, "investor_name": value.name, "email": value.email], forKey: "User")
            }
            else {
                UserDefaults.standard.set(nil, forKey: "User")
            }
        }
    }
    
    let id: Int
    let name: String
    let email: String
    
    required init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("investor_name")
        self.email = mapper.keyPath("email")
    }
}
