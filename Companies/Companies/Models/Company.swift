//
//  Company.swift
//  Companies
//
//  Created by Filipe Oliveira on 20/12/19.
//  Copyright © 2019 Filipe Oliveira. All rights reserved.
//

import Foundation

final class Company: Mappable {
    
    let id: Int
    let name: String
    let type: String
    let country: String
    let description: String
    var initials: String {
        var modifyName = name
        
        return "\(modifyName.removeFirst())\(modifyName.removeLast())".uppercased()
    }
    
    required init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("enterprise_name")
        self.type = mapper.keyPath("enterprise_type.enterprise_type_name")
        self.country = mapper.keyPath("country")
        self.description = mapper.keyPath("description")
    }
}
